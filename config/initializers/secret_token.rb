# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'a39b14e46f29009f008c31d342c01f64f8bba93bdf7e32353ad6d6bcb9732cd8d6bdb953126e00a924f339a7708da9edf92c0ac8a4930e915394de9a1ce55504'
